
numeroIndice = 1
palabra = "## "
remplazarPor = ""
with open("README.md") as fileR:
   for linea in fileR:
      if palabra in linea:
      	lineaCruda = linea

      	#Elimina salto de linea
      	if lineaCruda[-1] == '\n':
		lineaCruda = lineaCruda[:-1]
		
		#Elimina ultimo espaciado en texto
      	if lineaCruda[-1] == ' ':
		lineaCruda = lineaCruda[:-1]
			
      	tituloIndice = lineaCruda.replace(palabra,remplazarPor)
      	enlace = tituloIndice.replace(" ","-")
      	lineaIndice = str(numeroIndice)+'. '+'['+tituloIndice+']'+"(#"+enlace.lower()+')\n'
      	print lineaIndice
      	
      	numeroIndice= numeroIndice +1
      	
      	fileW = open ("Indice MD.txt", "a")
      	fileW.write(lineaIndice)
      	fileW.close()
