Generar tabla de contenidos (TOC) del archivo markdown hasta el nivel 1

## Modo de implementación

Colocar el archivo **Genera-TOC.py** en la misma ubicación donde se encuentra el README.MD para poder realizar el índice.

Ejecutar desde una terminal:
```
python Genera-TOC.py
```